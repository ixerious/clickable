from clickable import ProjectConfig
from clickable.commands.docker.docker_config import DockerConfig
from .docker_support import DockerSupport
import os
import getpass

class MultimediaSupport(DockerSupport):
    config = None

    def __init__(self, config: ProjectConfig):
        self.config = config

    def update(self, docker_config: DockerConfig):
        uid = os.getuid()
        user = getpass.getuser()
        
        # The path to the pulseaudio config is
        # different on some distributions
        pulse_dir_1 = '/run/{}/pulse'.format(uid) \
            if os.path.isdir('/run/{}/pulse'.format(uid)) \
            else '/run/user/{}/pulse'.format(uid)

        pulse_dir_2 = '/home/{}/pulse'.format(user) \
            if os.path.isdir('/home/{}/pulse'.format(user)) \
            else '/home/{}/.config/pulse'.format(user)

        docker_config.volumes.update({
            '/dev/shm': '/dev/shm',
            '/etc/machine-id': '/etc/machine-id',
            pulse_dir_1: '/run/user/1000/pulse',
            '/var/lib/dbus': '/var/lib/dbus',
            pulse_dir_2: '/home/phablet/.pulse',
            '/dev/snd': '/dev/snd',
        })

